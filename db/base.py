import sqlite3
from pathlib import Path


def init_db(file):
    global db, cursor
    DB_PATH = Path(__file__).parent.parent
    DB_NAME = 'db.sqlite'
    db = sqlite3.connect(DB_PATH/DB_NAME)
    cursor = db.cursor()


def create_tables():
    cursor.execute( """
        CREATE TABLE IF NOT EXISTS cars(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            description TEXT,
            price_usd INTEGER,
            title TEXT,
            url TEXT
        )
        """)
    db.commit()


def populate_tables():
    cursor.execute("""
        INSERT INTO cars(title, price_usd, description) 
        VALUES  ('BMW', '20_000', 'Кроссовер,                                         '
                 'белый,                                         3.5 '
                 'л.,                                         левый '
                 'руль,                                         '
                 'бензин,                                         '
                 'автомат,                                         полный '
                 'подключаемый')
                ('Mercedes', '30_000', 'Кроссовер,                                         '
                 'белый,                                         2 '
                 'л.,                                         левый '
                 'руль,                                         '
                 'дизель,                                         '
                 'автомат,                                         постоянный '
                 'полный,')
    """)
    db.commit()


def cars(data):
    cursor.execute(
        """
        INSERT INTO cars (title, price_usd, description) 
        VALUES (:title, :price_usd, :description)
        """,
        {'title': data['title'],
        'price_usd': data['price_usd'],
        'description': data['description']}
    )
    db.commit()

if __name__ == "__main__":
    db_file = "path/to/your/db.sqlite"
    init_db(db_file)
    create_tables()
    populate_tables()